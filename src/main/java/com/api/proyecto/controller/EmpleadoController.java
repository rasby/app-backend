package com.api.proyecto.controller;

import com.api.proyecto.dto.EmpleadoDto;
import com.api.proyecto.dto.Mensaje;
import com.api.proyecto.dto.ObraDto;
import com.api.proyecto.model.Empleado;
import com.api.proyecto.model.Obra;
import com.api.proyecto.service.EmpleadoService;
import com.api.proyecto.service.ObraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/empleados")
public class EmpleadoController {

    @Autowired
    EmpleadoService empleadoService;

    @GetMapping("/lista")
    public ResponseEntity<List<Empleado>> list(){
        List<Empleado> list = empleadoService.List();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }



    @PostMapping("/createEmpleado")
    public ResponseEntity<?>create(@RequestBody EmpleadoDto empleadoDto){
        Empleado empleado = new Empleado(empleadoDto.getIdentificacion(), empleadoDto.getNombres(), empleadoDto.getApellidos(), empleadoDto.getCorreo(), empleadoDto.getTelefono(), empleadoDto.getDireccion());
        empleadoService.save(empleado);
        return new ResponseEntity<>(new Mensaje("empleado creada"),HttpStatus.OK);
    }



    @DeleteMapping("/deleteEmpleado/{id}")
    public ResponseEntity<?> delete (@PathVariable("id")Long id){
        if(!empleadoService.existsById(id))
            return new ResponseEntity<>(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        empleadoService.delete(id);
        return new ResponseEntity<>(new Mensaje("empleado eliminado"),HttpStatus.OK);
    }


}
