package com.api.proyecto.dto;

import com.sun.istack.NotNull;

import java.util.Date;

public class ObraDto {

    @NotNull
    private String descripcion;
    @NotNull
    private Date fechaInicio;
    @NotNull
    private Date fecha_Final;
    @NotNull
    private String direccion;
    @NotNull
    private int nro_contrato;

    public ObraDto() {
    }

    public ObraDto(String descripcion, Date fechaInicio, Date fecha_Final, String direccion, int nro_contrato) {
        this.descripcion = descripcion;
        this.fechaInicio = fechaInicio;
        this.fecha_Final = fecha_Final;
        this.direccion = direccion;
        this.nro_contrato = nro_contrato;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFecha_Final() {
        return fecha_Final;
    }

    public void setFecha_Final(Date fecha_Final) {
        this.fecha_Final = fecha_Final;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getNro_contrato() {
        return nro_contrato;
    }

    public void setNro_contrato(int nro_contrato) {
        this.nro_contrato = nro_contrato;
    }
}


