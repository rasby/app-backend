package com.api.proyecto.controller;

import com.api.proyecto.dto.KitSecurityDto;
import com.api.proyecto.dto.Mensaje;
import com.api.proyecto.dto.ObraDto;
import com.api.proyecto.model.KitSecurity;
import com.api.proyecto.model.Obra;
import com.api.proyecto.service.KitSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.Clock;
import java.util.List;

/*Definimos en que url espondremos todos los servicios de esta clase*/
@RestController
@RequestMapping("/kitSeguridad")
public class KitSeguridadController {

    @Autowired
    private KitSecurityService kitSecurityService;

    @GetMapping("/listaKit")
    public ResponseEntity<List<KitSecurity>> list(){
        List<KitSecurity> list = kitSecurityService.List();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    //Guardar Persona metodo Post
    @PostMapping("/createKit")
    public ResponseEntity<?>create(@RequestBody KitSecurityDto kitSecurityDto){
        KitSecurity kitSecurity = new KitSecurity(kitSecurityDto.getName(), kitSecurityDto.getDescription());
        kitSecurityService.save(kitSecurity);
        return new ResponseEntity<>(new Mensaje("kit creada"), HttpStatus.OK);
    }

    //DELETE
    @DeleteMapping("/deleteKit/{id}")
    public ResponseEntity<?> delete (@PathVariable("id")int id){
        if(!kitSecurityService.existsById(id))
            return new ResponseEntity<>(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        kitSecurityService.delete(id);
        return new ResponseEntity<>(new Mensaje("obra eliminada"),HttpStatus.OK);
    }




}
