import React, { useEffect, useState } from "react";
import { Space, Table, Button } from "antd";
import BaseService from "../../../../services/base-service/base.sercive";
import { useHistory } from "react-router-dom";

/**
 * @method Component
 */
export const EmployerViewComponent = () => {
  //Hook
  const [data, setData] = useState<Array<any>>([]);
  const history = useHistory();
  //Service
  const baseService = new BaseService();

  useEffect(() => {
    const getData = async () => {
      setData(await baseService.GetAll("empleados/getEmpleados"));
    };
    getData();
  }, []);

  const columns = [
    {
      key: "nombres",
      title: "Nombres",
      dataIndex: "nombres",
    },
    {
      key: "apellidos",
      title: "Apellidos",
      dataIndex: "apellidos",
    },
    {
      key: "direccion",
      title: "Direccion",
      dataIndex: "direccion",
    },
    {
      key: "correo",
      title: "Correo",
      dataIndex: "correo",
    },
    {
      title: "accion",
      dataIndex: "accion",
      render: (text: any, record: any) =>
        data.length > 0 ? (
          <Space size="middle">
            <Button
              type="primary"
              danger
              onClick={async () => {
                await baseService.Delete(
                  {
                    id_empleado: 1,
                  },
                  `http://localhost:8888/empleados/deleteEmpleados/${record.id_empleado}`
                );
                history.push("/user");
              }}
            >
              Eliminar
            </Button>
          </Space>
        ) : null,
    },
  ];
  return (
    <div className="container-employer-view">
      <h1 className="title-employer-view">Lista de usuarios</h1>
      <Table columns={columns} dataSource={data.length !== 0 ? data : []} />
    </div>
  );
};
