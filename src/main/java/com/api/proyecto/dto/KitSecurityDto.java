package com.api.proyecto.dto;

import com.sun.istack.NotNull;

import java.util.Date;

public class KitSecurityDto {

    @NotNull
    private String name;
    @NotNull
    private String description;

    public KitSecurityDto() {
    }

    public KitSecurityDto(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
