import React, { useState } from "react";
import { Layout, Menu } from "antd";
import { ShopOutlined, UserOutlined } from "@ant-design/icons";
import { EmployerCreateComponent } from "./employer/employer-create/employer.create.component";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import { EmployerViewComponent } from "./employer/employer-view/employer-view";
import { WorkCreateComponent } from "../work/creaate-work/work.create.component";
import { WorkViewComponent } from '../work/view-work/work.view.component';
/**
 * Destructuring objetc
 */
const { Header, Content, Sider } = Layout;
const { SubMenu, Item } = Menu;

export const UserComponent = () => {
  //State
  const [collapsed, setCollapsed] = useState<boolean>(false);
  //handle Collapse
  const onCollapse = () => setCollapsed(!collapsed);
  //Render Html
  return (
    <Router>
      <div className="container-user">
        <Layout style={{ minHeight: "100vh" }}>
          <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
            <div className="logo" />
            <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
              <SubMenu key="1" icon={<UserOutlined />} title="Users">
                <Item key="2">
                  <Link to="/viewUser">View user</Link>
                </Item>
                <Item key="3">
                  <Link to="/createEmployer">Create users</Link>
                </Item>
              </SubMenu>
              <SubMenu key="sub1" icon={<ShopOutlined />} title="Work">
                <Item key="5">
                  <Link to="/createWork">Create Work</Link>
                </Item>
                <Item key="6">
                  <Link to="/viewWork">View Work</Link>
                </Item>
              </SubMenu>
            </Menu>
          </Sider>
          <Layout className="site-layout">
            <Header className="site-layout-background" style={{ padding: 0 }} />
            <Content style={{ margin: "0 16px" }}>
              <Switch>
                <Route exact path="/user" render={() => <h1>Hello world</h1>} />
                <Route
                  exact
                  path="/createEmployer"
                  component={EmployerCreateComponent}
                />
                <Route
                  exact
                  path="/createWork"
                  component={WorkCreateComponent}
                />
                <Route
                  exact
                  path="/viewUser"
                  component={EmployerViewComponent}
                />
                <Route
                  exact
                  path="/viewWork"
                  component={WorkViewComponent}
                />
              </Switch>
            </Content>
          </Layout>
        </Layout>
      </div>
    </Router>
  );
};
