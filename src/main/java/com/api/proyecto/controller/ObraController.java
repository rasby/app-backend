package com.api.proyecto.controller;

import com.api.proyecto.dto.Mensaje;
import com.api.proyecto.dto.ObraDto;
import com.api.proyecto.model.Obra;
import com.api.proyecto.service.ObraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/obra")
public class ObraController {

    @Autowired
    ObraService obraService;

    @GetMapping("/lista")
    public ResponseEntity<List<Obra>> list(){
        List<Obra> list = obraService.List();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }



    @PostMapping("/create")
    public ResponseEntity<?>create(@RequestBody ObraDto obraDto){
        Obra obra = new Obra(obraDto.getDescripcion(), obraDto.getFechaInicio(), obraDto.getFecha_Final(), obraDto.getDireccion(), obraDto.getNro_contrato());
        obraService.save(obra);
        return new ResponseEntity<>(new Mensaje("obra creada"),HttpStatus.OK);
    }



    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete (@PathVariable("id")int id){
        if(!obraService.existsById(id))
            return new ResponseEntity<>(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        obraService.delete(id);
        return new ResponseEntity<>(new Mensaje("obra eliminada"),HttpStatus.OK);
    }


}
