import {useCallback, useState} from 'react';
import BaseService from "../services/base-service/base.sercive";
import {IUserInterface} from "../Interfaces/user.interface";


export const useAuth = () => {
    const [response, setResponse] = useState<object>({
        message: "",
        loading: true
    });
    //Service
    const baseService = new BaseService();
    /**
     * login
     */
    const login = useCallback(async (userCredentials: IUserInterface) => {
        console.log(userCredentials);
        const responseAuth = await baseService.Post({
            nombreUsuario: userCredentials.nameUser,
            password: userCredentials.passwordUser
        }, "auth/signIn");
        console.log(responseAuth);

    }, [baseService]);
    return {
        login,
        response
    }
}
