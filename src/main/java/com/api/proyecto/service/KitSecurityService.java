package com.api.proyecto.service;

import com.api.proyecto.model.KitSecurity;
import com.api.proyecto.model.Obra;
import com.api.proyecto.repository.KitSecurityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class KitSecurityService {


        @Autowired
        KitSecurityRepository kitSecurityRepository;

    public List<KitSecurity> List(){
        return kitSecurityRepository.findAll();
    }
    public Optional<KitSecurity> getOne(int id){
        return kitSecurityRepository.findById(id);
    }


        public void save(KitSecurity kitSecurity){
            kitSecurityRepository.save(kitSecurity);
        }
        public void delete(int id){
            kitSecurityRepository.deleteById(id);
        }

        public boolean existsById(int id){
            return kitSecurityRepository.existsById(id);
        }

    }


