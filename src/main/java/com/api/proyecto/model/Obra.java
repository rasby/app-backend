package com.api.proyecto.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Obra {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String descripcion;
    private Date fechaInicio;
    private Date fecha_Final;
    private String direccion;
    private int nro_contrato;


    public Obra() {}


    public Obra(String descripcion, Date fechaInicio, Date fecha_Final, String direccion, int nro_contrato) {
        this.descripcion = descripcion;
        this.fechaInicio = fechaInicio;
        this.fecha_Final = fecha_Final;
        this.direccion = direccion;
        this.nro_contrato = nro_contrato;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFecha_Final() {
        return fecha_Final;
    }

    public void setFecha_Final(Date fecha_Final) {
        this.fecha_Final = fecha_Final;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getNro_contrato() {
        return nro_contrato;
    }

    public void setNro_contrato(int nro_contrato) {
        this.nro_contrato = nro_contrato;
    }
}
