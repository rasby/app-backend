package com.api.proyecto.service;

import com.api.proyecto.model.Empleado;
import com.api.proyecto.model.Obra;
import com.api.proyecto.repository.EmpleadoRepository;
import com.api.proyecto.repository.ObraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class EmpleadoService {
    @Autowired
    EmpleadoRepository empleadoRepository;

    public List<Empleado> List(){
        return empleadoRepository.findAll();
    }
    public Optional<Empleado> getOne(Long id){
        return empleadoRepository.findById(id);
    }


    public void save(Empleado empleado){
        empleadoRepository.save(empleado);
    }
    public void delete(long id){
        empleadoRepository.deleteById(id);
    }

    public boolean existsById(long id){
        return empleadoRepository.existsById(id);
    }

}
