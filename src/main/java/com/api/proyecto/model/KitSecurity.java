package com.api.proyecto.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class KitSecurity {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private int id_kit;
        private String name;
        private String descripcion;

        public KitSecurity() {
        }

        public KitSecurity(String name, String descripcion) {
                this.name = name;
                this.descripcion = descripcion;
        }



        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getDescripcion() {
                return descripcion;
        }

        public void setDescripcion(String descripcion) {
                this.descripcion = descripcion;
        }
}
