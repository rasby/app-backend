package com.api.proyecto.repository;

import com.api.proyecto.model.KitSecurity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KitSecurityRepository extends JpaRepository<KitSecurity, Integer> {
}
