package com.api.proyecto.dto;

import com.sun.istack.NotNull;

import java.util.Date;

public class EmpleadoDto {

    @NotNull
    private String identificacion;
    @NotNull
    private String nombres;
    @NotNull
    private String apellidos;
    @NotNull
    private String correo;
    @NotNull
    private int telefono;
    @NotNull
    private String direccion;

    public EmpleadoDto() {
    }

    public EmpleadoDto(String identificacion, String nombres, String apellidos, String correo, int telefono, String direccion) {
        this.identificacion = identificacion;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.telefono = telefono;
        this.direccion = direccion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}


