export const columns: any = [
  {
    key: 'nombres',
    title: 'Nombres',
    dataIndex: 'nombres',
  },
  {
    key: 'apellidos',
    title: 'Apellidos',
    dataIndex: 'apellidos',
  },
  {
    key: 'direccion',
    title: 'Direccion',
    dataIndex: 'direccion',
  },
  {
    key: 'correo',
    title: 'Correo',
    dataIndex: 'correo',
  },
];