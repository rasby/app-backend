package com.api.proyecto.repository;

import com.api.proyecto.model.Empleado;
import com.api.proyecto.model.Obra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, Long> {}
