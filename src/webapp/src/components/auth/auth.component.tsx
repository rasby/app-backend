import React from "react";
import { AuthFormComponent } from "./auth.form";
import "./auth.style.css";

/**
 * @method Component Auth
 */
export const AuthComponent = () => {
  return (
    <div className="container-form-auth">
      <div className="color-one">
        <AuthFormComponent />
      </div>
      <div className="color-two"></div>
    </div>
  );
};
