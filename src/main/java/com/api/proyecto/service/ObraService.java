package com.api.proyecto.service;

import com.api.proyecto.model.Obra;
import com.api.proyecto.repository.ObraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ObraService {
    @Autowired
    ObraRepository obraRepository;

    public List<Obra> List(){
        return obraRepository.findAll();
    }
    public Optional<Obra> getOne(int id){
        return obraRepository.findById(id);
   }


    public void save(Obra obra){
        obraRepository.save(obra);
    }
    public void delete(int id){
        obraRepository.deleteById(id);
    }

    public boolean existsById(int id){
        return obraRepository.existsById(id);
    }

}
